package com.example.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.example.entity.Passenger;
import com.example.repository.PassengerRepository;



@RestController
@RequestMapping("/user")
public class PassengerController {
	
	@Autowired
	PassengerRepository passengerRepository;
	
	
	//for login page
	@GetMapping(value="/login") 
	 public ModelAndView Page(ModelAndView mv) {				
		  mv.setViewName("login"); 
		  return mv;
		  }
	
	
	//for home page
	@GetMapping(value="/home") 
	 public ModelAndView HomePage(ModelAndView mv) {				
		  mv.setViewName("dashboard"); 
		  return mv;
		  }
	
	
	//for regestring user	
	   @RequestMapping(value="/login/save",method=RequestMethod.POST)
	   public RedirectView Registration(@ModelAttribute Passenger passenger,HttpServletRequest request) {
		   System.out.println(passenger);
		   passengerRepository.save(passenger);
		   RedirectView rv=new RedirectView();
		   rv.setUrl(request.getContextPath()+"/user/home");
		   return rv;
		   
	   }
	   
	   //for sign in user
	   @RequestMapping(value="/login/dashboard",method = RequestMethod.POST)
	   public RedirectView LoginPage(@ModelAttribute Passenger passenger,HttpServletRequest request) {
		   RedirectView rv=new RedirectView();
		   System.out.println(passenger);
		   String email = passenger.getEmail();
	        String password = passenger.getPassword();
	        if(passengerRepository.existsByEmail(email)) {
	        	rv.setUrl(request.getContextPath()+"/user/home");
	 		   return rv;
	        }else {
	        	rv.setUrl(request.getContextPath()+"/user/login");
		 		   return rv;
	        }

	   }
	
}