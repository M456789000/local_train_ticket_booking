package com.example.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.entity.Train;

@Repository
public interface TrainRepository extends CrudRepository <Train,Integer>{
	
	//get all train
	 public List<Train> findAll();
	 	 
	// find train by trainNo
	    @Query(value="select trainId,station1,station2,station3,station4,station5,d_a_t_e from train where trainId=:trainId",nativeQuery=true)
	    public Train selectTrain(@Param("trainId")int trainId);

}
