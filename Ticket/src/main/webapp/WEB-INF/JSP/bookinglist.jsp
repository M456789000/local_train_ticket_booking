<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
         <link href="/CSS/trainlist.css" type="text/css" rel="stylesheet" />
  <title>Booking List</title>
  </head>
  <body>
    <div><h1>Booking List</h1></div>
    <div style="text-align:right"><h2><a href="/user/home">Home</a></h2></div>
   
    <table border="3">
    <thead>
      <tr>      
        <th>TICKET NUMBER</th>
        <th>FROM STATION</th>
        <th>TO STATION</th>
        <th>Cancel Ticket</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="bl" items="${BookingList}">
      <tr>
        <td>${bl.ticketno}</td>
        <td>${bl.fromstation}</td>
        <td>${bl.tostation}</td>
        <td>
        <a href="/delete/+${bl.ticketno}">Cancel</a>
        </td>
              
      </tr>
      </c:forEach>
      </tbody>     
    </table>
    
     
  </body>
</html>